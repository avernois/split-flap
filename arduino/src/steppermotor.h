#ifndef __have__StepperMotor_h__
#define __have__StepperMotor_h__

class StepperMotor {

    public:

    typedef enum { FORWARD, BACKWARD } Direction;

    StepperMotor();
    StepperMotor(Direction direction);
    void setCurrentPostion(long position);
    int* nextMove(long targetStep);
    bool hasReached(long targetStep);

    private:
    int currentStep = 0;
    long currentPosition = 0;
    Direction direction;
};

#endif