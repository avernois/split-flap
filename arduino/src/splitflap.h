#ifndef __have__SplitFlap_h__
#define __have__SplitFlap_h__

#include "steppermotor.h"

class SplitFlap {

    public: 
    typedef struct { int latch; int data; int clock;} RegisterPins;
    SplitFlap(int nbMotors, StepperMotor *motors[], int hallSensorPins[], long offsets[], const RegisterPins pins);
    void zero();
    void display(int number);
    void displayNonBlocking(int number);

    private: 
    StepperMotor **motors;
    long *offsets;
    int nbMotors;
    int *hallSensorPins;
    int *previousHallReads;
    RegisterPins pins;

    void moveToTarget(long *targetSteps);
    void moveToNextStep(long *targetSteps);
};

#endif