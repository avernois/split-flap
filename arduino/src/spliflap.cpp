#include <Arduino.h>
#include "splitflap.h"

void sendData (int* datas, SplitFlap::RegisterPins pins);
bool hasAllMotorReachedTheirTarget(int nbMotors, StepperMotor *motors[], long targets[]);

SplitFlap::SplitFlap(int nbMotors, StepperMotor *motors[], int hallSensorPins[], long offsets[], const RegisterPins pins) {

    this->nbMotors = nbMotors;
    this->motors = motors;
    this->offsets = offsets;
    this->hallSensorPins = hallSensorPins;
    this->pins = pins;
    this->previousHallReads = new int[nbMotors];
    
    Serial.print("nbMotors: ");
    Serial.println(this->nbMotors);

    for(int i=0 ; i < this->nbMotors; i++) {
        char buffer[64];
        sprintf(buffer, "Motor [%d]: \n\tsensor: %d\n\toffset: %lu", i, this->hallSensorPins[i], this->offsets[i]);
        Serial.println(buffer);
        this->previousHallReads[i] = digitalRead(this->hallSensorPins[i]);
    }
}

void SplitFlap::moveToNextStep(long *targetSteps) {

    for(int motor=this->nbMotors -1 ; motor >= 0; motor--) {
        int current = digitalRead(this->hallSensorPins[motor]);

        if(this->previousHallReads[motor] == 1 && current == 0) {
            char buffer[24];
            sprintf(buffer, "Reseting motor[%d][%d]", motor, this->hallSensorPins[motor]);
            Serial.println(buffer);
            motors[motor]->setCurrentPostion(offsets[motor]);
        }

        this->previousHallReads[motor] = current;
        
        int* steps = motors[motor]->nextMove(targetSteps[motor]);
        sendData(steps, this->pins);
    }

    digitalWrite(pins.latch, HIGH);
    digitalWrite(pins.latch, LOW);
}

void SplitFlap::moveToTarget(long *targetSteps) {
    int delay = 1200;
    int minDelay = 1000;
    int delayStep = 1;

    while(!hasAllMotorReachedTheirTarget(this->nbMotors, this->motors, targetSteps)) {
        moveToNextStep(targetSteps);
       
        delayMicroseconds(delay);
        
        if (delay > minDelay)
            delay = delay - delayStep;
    }
}

void SplitFlap::zero() {
    long targetSteps[nbMotors];

    for (int motor = 0; motor < nbMotors; motor++)
        targetSteps[motor] = 2047L;
    

    this->moveToTarget(targetSteps);

    this->display(0);
}

int int_pow(int base, int exponent) {     
    int pow = 1;

    for(int i = 0; i< exponent; i++)
        pow *= base;

    return pow;
}

void SplitFlap::display(int number) {
    long targetSteps[nbMotors];
    for (int motor = 0; motor < this->nbMotors; motor++) {
       targetSteps[motor] = 4096L * ( (number / int_pow(10, motor)) % 10) / 24;
    }
    
    this->moveToTarget(targetSteps);
}

void SplitFlap::displayNonBlocking(int number) {
    long targetSteps[nbMotors];
    for (int motor = 0; motor < this->nbMotors; motor++) {
       targetSteps[motor] = 4096L * ((number / int_pow(10, motor)) % 10) / 24;
    }
    
    this->moveToNextStep(targetSteps);
}

void sendData (int* datas, SplitFlap::RegisterPins pins) {
    for(int p = 0; p < 4; p++) {
        digitalWrite(pins.data, datas[p]);
        digitalWrite(pins.clock, HIGH);
        digitalWrite(pins.clock, LOW);
    }
}

bool hasAllMotorReachedTheirTarget(int nbMotors, StepperMotor *motors[], long *targets) {
    bool reached = true;

    for(int i=0; i < nbMotors; i++) {
        reached = reached && motors[i]->hasReached(targets[i]);
    }

    return reached;
}

