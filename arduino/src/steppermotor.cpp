#include "steppermotor.h"
#include "Arduino.h"

const int nbSteps = 8;
int steps[nbSteps][4] = {
    { 1, 0, 0, 0 },
    { 1, 0, 1, 0 },
    { 0, 0, 1, 0 },
    { 0, 1, 1, 0 },
    { 0, 1, 0, 0 },
    { 0, 1, 0, 1 },
    { 0, 0, 0, 1 },
    { 1, 0, 0, 1 }
};

int backward_steps[nbSteps][4] = {
    { 0, 0, 0, 1 },
    { 0, 1, 0, 1 },
    { 0, 1, 0, 0 },
    { 0, 1, 1, 0 },
    { 0, 0, 1, 0 },
    { 1, 0, 1, 0 },
    { 1, 0, 0, 0 },
    { 1, 0, 0, 1 }
};

int stepZero[4] = {0, 0, 0, 0};

StepperMotor::StepperMotor() {
    this->direction = FORWARD;
}

StepperMotor::StepperMotor(StepperMotor::Direction direction) {
    this->direction = direction;
}

void StepperMotor::setCurrentPostion(long position) {
    char buffer[64];
    sprintf(buffer, "Setting current position to %ld (currently at %ld)", position, this->currentPosition);
    Serial.println(buffer);
    this->currentPosition = position;
}

int* StepperMotor::nextMove(long targetPosition) {

    if (targetPosition == this->currentPosition)
        return stepZero;

    int *next;
    if (this->direction == FORWARD)
        next = steps[this->currentStep%8];
    else
        next = backward_steps[this->currentStep%8];

    this->currentPosition = (this->currentPosition + 1)%2048;
    this->currentStep = (this->currentStep + 1)%nbSteps;

    return next;
}

bool StepperMotor::hasReached(long targetPosition) {
    return this->currentPosition == targetPosition;
}

