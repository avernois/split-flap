#include <Arduino.h>

#include <NTPClient.h> 
#include <WiFiUdp.h>
#include <WiFi.h>

#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>         //https://github.com/tzapu/WiFiManager


#include "steppermotor.h"
#include "splitflap.h"


int hall_0 = 32;
int hall_1 = 33;
int hall_2 = 23;
int hall_3 = 22;
int hall_4 = 17;
int hall_5 = 18;


int data  = 25;
int latch = 26;
int clock_pin = 27;


WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600 *1, 60*60*1000);

AsyncWebServer server(80);
DNSServer dns;

const int nbMotors = 6;
StepperMotor *motors[nbMotors];

SplitFlap *splitFlap;

int previous = 0;

void setup() {    
    Serial.begin(9600);
    
    int *hallSensorPins = new int[nbMotors]{ hall_0, hall_1, hall_2, hall_3, hall_4, hall_5 };
    for(int motor = 0; motor < nbMotors; motor++)
        pinMode(hallSensorPins[motor], INPUT_PULLUP);

    pinMode(data, OUTPUT);
    pinMode(latch, OUTPUT);
    pinMode(clock_pin, OUTPUT);
    
    motors[0] = new StepperMotor(StepperMotor::BACKWARD);
    motors[1] = new StepperMotor(StepperMotor::BACKWARD);
    motors[2] = new StepperMotor();
    motors[3] = new StepperMotor();
    motors[4] = new StepperMotor();
    motors[5] = new StepperMotor();
    
    long *offsets = new long[nbMotors]{ 70L, 70L, 70L, 20L, 40L, 20L };
    SplitFlap::RegisterPins pins = { latch, data, clock_pin };
    splitFlap = new SplitFlap(nbMotors, motors, hallSensorPins, offsets, pins);

    Serial.println("Looking for zero");

    splitFlap->zero();
    AsyncWiFiManager wifiManager(&server,&dns);
    wifiManager.autoConnect("AutoConnectAP");
    Serial.println("connected...yeey :");

    timeClient.begin();
    
    
    
    Serial.println("End of setup, let's start");
}

int displayNumber = 0;

void loop() {

    timeClient.update();
    int display = (timeClient.getHours()*10000 + timeClient.getMinutes()*100 + timeClient.getSeconds());

    if (display != displayNumber) {
        Serial.print("Displaying: ");
        Serial.println(display);
        displayNumber = display;
    }
    
    splitFlap->displayNonBlocking(display);
    delayMicroseconds(1250);
}